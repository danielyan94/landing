$(document).ready(function () {
    $('#product_qty').on('change keyup', function () {
        var qty = $(this).val();
        $.get('/change/product/qty/' + qty, function (total) {
            $('.total').text(total);
            $('.cart-item-count').text(qty);
        })
    });
});