<?php

namespace App\Http\Controllers;

use App\Order;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CronController extends Controller
{
    public static function checkOrders()
    {
        $today = Carbon::today();
        $yesterday = Carbon::yesterday();

        $users = User::all();

        $path = storage_path('reports/');
        $file_name = $yesterday->toDateString() .'.txt';

        $file = fopen($path.$file_name, 'w');

        foreach ($users as $user){
            $cart = $user->cart;

            $cart_qty = $cart ? $cart->qty : 0;
            $orders = Order::where('user_id', $user->id)->whereBetween('updated_at', array($yesterday, $today))->get();
            $products_qty= 0;
            foreach ($orders as $order){
                $products_qty += $order->qty;
            }

            $report = $user->name .' has '. $cart_qty .' products in cart, ordered '. count($orders) .' times, and '. $products_qty .' products in total'. PHP_EOL;
            fwrite($file, $report);
        }

        fclose($file);
    }
}
