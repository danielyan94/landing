<?php

namespace App\Http\Controllers;

use App\Payment;
use App\PaymentConfig;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Srmklive\PayPal\Facades\PayPal;

class PaymentController extends Controller
{
    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function buyCoins($id)
    {
        $config_ids = PaymentConfig::all()->pluck('id')->toArray();
        if (!in_array($id, $config_ids)) return back();

        $config = PaymentConfig::find($id);
        $this->addUserCoins($config);

        return back();
    }

    /**
     * @param PaymentConfig $config
     */
    public function addUserCoins(PaymentConfig $config)
    {
        $user = Auth::user();
        $user->coins += $config->coins;
        $user->save();

        $payment = new Payment();
        $payment->user_id = $user->id;
        $payment->amount = $config->cost;
        $payment->currency = $config->currency;
        $payment->description = 'Bought ' . $config->coins . ' coins';
        $payment->save();
    }
}
