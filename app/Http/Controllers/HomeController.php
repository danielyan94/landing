<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Order;
use App\PaymentConfig;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public $product_price;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index');
        $this->product_price = env('PRODUCT_PRICE', 10);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $payment_configs = PaymentConfig::all();
        return view('home')->with(compact('payment_configs'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function addToCart()
    {
        $user_id = Auth::id();
        $cart = Cart::where('user_id', $user_id)->first();

        if (!$cart) {
            $cart = new Cart();
            $cart->user_id = $user_id;
            $cart->qty = 1;
        }else{
            $cart->qty++;
        }

        $cart->save();

        return redirect('/cart');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cart()
    {
        $cart = Auth::user()->cart;
        $product_price = $this->product_price;

        return view('cart')->with(compact('cart', 'product_price'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function order()
    {
        $user = Auth::user();
        $cart = Cart::where('user_id', $user->id)->first();

        if (!$cart) return back();

        $total = (int) $cart->qty * $this->product_price;

        if ($total > $user->coins) return redirect('/home');

        $order = new Order();
        $order->user_id = $cart->user_id;
        $order->qty = $cart->qty;
        $order->total = $total;
        $order->save();

        $cart->delete();
        $user->coins -= $total;
        $user->save();

        return redirect('/order/success');

    }

    /**
     * @param $qty
     * @return float|int
     */
    public function changeProductQty($qty)
    {
        $cart = Auth::user()->cart;
        $cart->qty = $qty;
        $cart->save();

        $total = $this->product_price * $qty;
        return $total;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function orderSuccess()
    {
        return view('order-success');
    }
}
