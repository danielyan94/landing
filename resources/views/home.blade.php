@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    Balance - <span>{{ Auth::user()->coins }} coins</span>
                </div>
            </div>
        </div>
        <div id="pricing" class="pricing-section text-center">
            <div class="container">
                @foreach($payment_configs as $config)
                    <div class="col-sm-6">
                        <div class="table-left wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                            <div class="icon"> <i class="ion-ios-paperplane-outline"></i> </div>
                            <div class="pricing-details">
                                <h2>{{ $config->coins }} Coins</h2>
                                <span>{{ $config->cost . ' ' . $config->currency }}</span>
                                <ul>
                                    <li>Pay Pal</li>
                                </ul>
                                <a href="{{ route('buy.coins', ['id' => $config->id]) }}" class="btn btn-primary btn-action btn-fill">Get Coins</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
