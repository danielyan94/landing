@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Cart</div>

                    <div class="card-body">
                        Balance - <span>{{ Auth::user()->coins }} coins</span>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Quantity</th>
                                <th>Price (coins)</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if($cart)
                                    <tr>
                                        <td>Apple watch</td>
                                        <td><input id="product_qty" type="number" class="form-control" value="{{ $cart->qty }}" min="1"></td>
                                        <td>{{ $product_price }}</td>
                                        <td class="total">{{ $product_price * $cart->qty }}</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>

                        @if($cart)
                            <a href="{{ route('order') }}" class="btn btn-action">Order now</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/jquery-2.1.1.js') }}"></script>
    <script src="{{ asset('js/cart.js') }}"></script>
@endpush