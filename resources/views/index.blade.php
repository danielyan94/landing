<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>iLand Multipurpose Landing Page Template</title>
    <link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/png" sizes="16x16">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="iLand Multipurpose Landing Page Template">
    <meta name="keywords" content="iLand HTML Template, iLand Landing Page, Landing Page Template">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="https://fonts.googleapis.com/css?family=Open%20Sans:300,400,500,600,700" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <!-- Resource style -->
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}">
    <!-- Resource style -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
</head>
<body>
<div class="wrapper">
    <div class="container">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    <a class="navbar-brand page-scroll" href="#main"><img src="{{ asset('images/logo.png') }}" width="80" height="30" alt="iLand" /></a> </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a class="page-scroll" href="#main">Product</a></li>
                        <li><a class="page-scroll" href="#features">Features</a></li>
                        @auth
                            <li><a href="{{ url('/home') }}">Profile</a></li>
                        @else
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @endauth
                    </ul>
                </div>
            </div>
        </nav>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->

    <div class="main app form" id="main"><!-- Main Section-->
        <div class="hero-section">
            <div class="container nopadding">
                <div class="col-md-5"> <img class="img-responsive wow fadeInUp" data-wow-delay="0.1s" src="images/app-signup.png" alt="App" /> </div>
                <div class="col-md-7">
                    <div class="hero-content">
                        <h1 class="wow fadeInUp" data-wow-delay="0.1s">Landing Page for Apple Watch</h1>
                        <p class="wow fadeInUp" data-wow-delay="0.2s"> Beoplay A1 bluetooth speakers image on the left is owned by Bang and Olufsen and is released under creative
                            commons lisence. </p>
                        <div class="sub-form wow fadeInUp" data-wow-delay="0.3s">
                            <a href="{{ route('cart.add') }}" class="btn btn-action wow fadeInUp">Buy now</a>
                            <!-- subscribe message -->
                            <div id="mesaj"></div>
                            <!-- subscribe message -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Client Section -->
        <div class="client-section">
            <div class="container text-center">
                <div class="clients owl-carousel owl-theme">
                    <div class="single"> <img src="images/client1.png" alt="Image" /> </div>
                    <div class="single"> <img src="images/client2.png" alt="Image" /> </div>
                    <div class="single"> <img src="images/client3.png" alt="Image" /> </div>
                    <div class="single"> <img src="images/client1.png" alt="Image" /> </div>
                    <div class="single"> <img src="images/client2.png" alt="Image" /> </div>
                    <div class="single"> <img src="images/client3.png" alt="Image" /> </div>
                </div>
            </div>
        </div>
        <div class="app-features text-center" id="features">
            <div class="container">
                <h1 class="wow fadeInDown" data-wow-delay="0.1s">Features & Overviews</h1>
                <p class="wow fadeInDown" data-wow-delay="0.2s"> Aliquam sagittis ligula et sem lacinia, ut facilisis enim sollicitudin. Proin nisi est,<br class="hidden-xs">
                    convallis nec purus vitae, iaculis posuere sapien. </p>
                <div class="col-md-4 features-left">
                    <div class="col-md-12 wow fadeInDown" data-wow-delay="0.2s">
                        <div class="icon"> <i class="ion-ios-analytics-outline"></i> </div>
                        <div class="feature-single">
                            <h1>Lorem Ipsum</h1>
                            <p> Some lorem contetnt to fill the gap and make it look clean and organized. </p>
                        </div>
                    </div>
                    <div class="col-md-12 wow fadeInDown" data-wow-delay="0.3s">
                        <div class="icon"> <i class="ion-ios-briefcase-outline"></i> </div>
                        <div class="feature-single">
                            <h1>Lorem Ipsum</h1>
                            <p> Some lorem contetnt to fill the gap and make it look clean and organized. </p>
                        </div>
                    </div>
                    <div class="col-md-12 wow fadeInDown" data-wow-delay="0.4s">
                        <div class="icon"> <i class="ion-ios-chatboxes-outline"></i> </div>
                        <div class="feature-single">
                            <h1>Lorem Ipsum</h1>
                            <p> Some lorem contetnt to fill the gap and make it look clean and organized. </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 wow fadeInDown" data-wow-delay="0.5s"> <img class="img-responsive" src="images/iPhone-app.png" alt="App" /> </div>
                <div class="col-md-4 features-left">
                    <div class="col-md-12 wow fadeInDown" data-wow-delay="0.6s">
                        <div class="icon"> <i class="ion-ios-cloud-download-outline"></i> </div>
                        <div class="feature-single">
                            <h1>Lorem Ipsum</h1>
                            <p> Some lorem contetnt to fill the gap and make it look clean and organized. </p>
                        </div>
                    </div>
                    <div class="col-md-12 wow fadeInDown" data-wow-delay="0.7s">
                        <div class="icon"> <i class="ion-ios-copy-outline"></i> </div>
                        <div class="feature-single">
                            <h1>Lorem Ipsum</h1>
                            <p> Some lorem contetnt to fill the gap and make it look clean and organized. </p>
                        </div>
                    </div>
                    <div class="col-md-12 wow fadeInDown" data-wow-delay="0.8s">
                        <div class="icon"> <i class="ion-ios-game-controller-b-outline"></i> </div>
                        <div class="feature-single">
                            <h1>Lorem Ipsum</h1>
                            <p> Some lorem contetnt to fill the gap and make it look clean and organized. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="split-features">
            <div class="col-md-6 nopadding">
                <div class="split-image"> <img class="img-responsive wow fadeIn" src="images/app_image.png" alt="Image" /> </div>
            </div>
            <div class="col-md-6 nopadding">
                <div class="split-content">
                    <h1 class="wow fadeInUp">Designed for effective product landing page</h1>
                    <p class="wow fadeInUp"> Pellentesque eget dolor gravida, tempus purus ac, ultricies mauris. Etiam est nisl, molestie sed egestas bibendum, varius eu diam. Suspendisse est metus, ultrices sit amet dolor in, rhoncus malesuada mi.</p>
                    <ul class="wow fadeInUp">
                        <li>Nulla ornare purus non consequat ultricies.</li>
                        <li>Etiam est nisl, molestie sed egestas bibendum</li>
                        <li>Aliquam vel euismod elit, sed suscipit est.</li>
                        <li>Curabitur egestas justo neque viverra vel. </li>
                    </ul>
                </div>
            </div>
        </div>


        <div class="split-features2">

            <div class="col-md-6 nopadding">
                <div class="split-content second">
                    <h1 class="wow fadeInUp">The best way to show your product to the world</h1>
                    <p class="wow fadeInUp"> Pellentesque eget dolor gravida, tempus purus ac, ultricies mauris. Etiam est nisl, molestie sed egestas bibendum, varius eu diam. Suspendisse est metus, ultrices sit amet dolor in, rhoncus malesuada mi.</p>
                    <ul class="wow fadeInUp">
                        <li>Nulla ornare purus non consequat ultricies.</li>
                        <li>Etiam est nisl, molestie sed egestas bibendum</li>
                        <li>Aliquam vel euismod elit, sed suscipit est.</li>
                        <li>Curabitur egestas justo neque viverra vel. </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 nopadding">
                <div class="split-image"> <img class="img-responsive wow fadeIn" src="images/app_image2.png" alt="Image" /> </div>
            </div>
        </div>



        <div class="pitch text-center">
            <div class="container">
                <div class="pitch-intro">
                    <h1 class="wow fadeInDown" data-wow-delay="0.2s">More Awesome Features</h1>
                    <p class="wow fadeInDown" data-wow-delay="0.2s"> Pellentesque eget dolor gravida, tempus purus ac, ultricies mauris. Etiam est nisl, molestie sed egestas bibendum, varius eu diam. Suspendisse est metus, ultrices sit amet dolor in, rhoncus malesuada mi.</p>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4 wow fadeInDown" data-wow-delay="0.2s">
                        <div class="pitch-icon"> <i class="ion-ios-checkmark-outline"></i> </div>
                        <div class="pitch-content">
                            <h1>Great Options</h1>
                            <p> Aliquam vel euismod elit, sed suscipit est. Sed tincidunt venenatis ligula ac luctus. Fusce egestas volutpat mi sed pellentesque. </p>
                        </div>
                    </div>
                    <div class="col-md-4 wow fadeInDown" data-wow-delay="0.2s">
                        <div class="pitch-icon"> <i class="ion-ios-mic-outline"></i> </div>
                        <div class="pitch-content">
                            <h1>Voice Callings</h1>
                            <p> Aliquam vel euismod elit, sed suscipit est. Sed tincidunt venenatis ligula ac luctus. Fusce egestas volutpat mi sed pellentesque. </p>
                        </div>
                    </div>
                    <div class="col-md-4 wow fadeInDown" data-wow-delay="0.2s">
                        <div class="pitch-icon"> <i class="ion-ios-folder-outline"></i> </div>
                        <div class="pitch-content">
                            <h1>Sharing Files</h1>
                            <p> Aliquam vel euismod elit, sed suscipit est. Sed tincidunt venenatis ligula ac luctus. Fusce egestas volutpat mi sed pellentesque.  </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="feature-sub">
            <div class="container">
                <div class="sub-inner">
                    <h1 class="wow fadeInUp">Creative Landing App For Your Easy Life! Simple Reliable & Understandable for your customers </h1>
                    <a href="{{ route('cart.add') }}" class="btn btn-action wow fadeInUp">Buy now</a> </div>
            </div>
        </div>



        <!-- Footer Section -->
        <div class="footer">
            <div class="container">
                <div class="col-md-7"> <img src="images/logo.png" width="80" height="30" alt="Image" />
                    <p> Lorem ipsum dolor sit. Incidunt laborum beatae earum nihil odio consequatur officiis
                        tempore consequuntur officia ducimus unde doloribus quod unt repell  </p>
                    <div class="footer-text">
                        <p> Copyright © 2016 iLand. All Rights Reserved. Made with <i class="ion-heart"></i> by <a href="http://www.designstub.com/">Designstub</a></p>
                    </div>
                </div>
                <div class="col-md-5">
                    <h1>Contact Us</h1>
                    <p> Contact our 24/7 customer support if you have any <br class="hidden-xs">
                        questions. We'll help you out. </p>
                    <a href="mailto:support@gmail.com">contact@iland.com</a> </div>
            </div>
        </div>
    </div>

    <!-- Scroll To Top -->

    <a id="back-top" class="back-to-top page-scroll" href="#main"> <i class="ion-ios-arrow-thin-up"></i> </a>

    <!-- Scroll To Top Ends-->

</div>
<!-- Main Section -->
</div>
<!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<script type="text/javascript" src="{{ asset('js/jquery-2.1.1.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/plugins.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/menu.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
<script src="{{ asset('js/jquery.subscribe.js') }}"></script>
</body>
</html>
