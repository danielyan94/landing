<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentConfigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_configs')->insert([
            [
                'coins' => 10,
                'cost' => 100,
            ],
            [
                'coins' => 20,
                'cost' => 180,
            ]
        ]);
    }
}
