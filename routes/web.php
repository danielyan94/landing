<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@home')->name('home');

Route::middleware('auth')->group(function (){
    Route::get('/buy/coins/{id}', 'PaymentController@buyCoins')->name('buy.coins');
    Route::get('/coins/payment/success/{id}', 'PaymentController@coinsPaymentSuccess')->name('coins.payment.success');
    Route::get('/cart', 'HomeController@cart')->name('cart');
    Route::get('/cart/add', 'HomeController@addToCart')->name('cart.add');
    Route::get('/order', 'HomeController@order')->name('order');
    Route::get('/order/success', 'HomeController@orderSuccess')->name('order.success');
    Route::get('/change/product/qty/{qty}', 'HomeController@changeProductQty');
});
